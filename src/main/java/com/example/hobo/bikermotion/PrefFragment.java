package com.example.hobo.bikermotion;

import android.os.Bundle;
import android.preference.PreferenceFragment;

import com.example.hobo.bilermotion2.R;

public class PrefFragment extends PreferenceFragment {

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.user_pref);
    }}