package com.example.hobo.bikermotion;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.example.hobo.bilermotion2.R;

import java.util.ArrayList;

/**
 * Created by hobo on 01.04.2015.
 */
public class ActivityUsersList extends Activity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_with_bikers);

        //set the list with users
        ArrayList <String> users_list = (ArrayList<String>)getIntent().getSerializableExtra("USERS_LIST");

        // get the ListViev
        ListView lvMain = (ListView) findViewById(R.id.lvMain);

        // Create the adapter
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_1, users_list);

        // set adapter to ListViev
        lvMain.setAdapter(adapter);

        //set click listener for back result to MapsActivaty
        lvMain.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String item = (String) parent.getItemAtPosition(position);
                Intent intent = new Intent();
                intent.putExtra("NICK", item);
                setResult(RESULT_OK, intent);
                finish();
            }
        });
    }
}
