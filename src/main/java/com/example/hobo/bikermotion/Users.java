package com.example.hobo.bikermotion;

import com.google.android.gms.maps.model.LatLng;

import java.util.TreeMap;

/**
 * Created by hobo on 27.03.2015.
 */
public class Users {

    private LatLng coordinate;
    private int status;
    private String nick_name;
    private String sub_information;
    private String nick_and_status;
    private TreeMap<Integer,String> map_settings_auto_statuses = new TreeMap<Integer,String>();


    Users(){}

    Users(LatLng coordinate, int status, TreeMap<Integer, String> map_settings_auto_statuses, String... args){
        this.coordinate = coordinate;
        this.status = status;
        this.map_settings_auto_statuses = map_settings_auto_statuses;
        this.nick_name = args[0];
        this.sub_information = args[1];
    }

    public LatLng getCoordinate() {
        return coordinate;
    }

    public String getNick_name() {
        return nick_name;
    }

    public String getSub_information() {
        return sub_information;
    }

    public int getStatus() {
        return status;
    }

    public void setCoordinate(LatLng coordinate) {
        this.coordinate=coordinate;
    }

    public void setNick_name(String nick_name) {
        this.nick_name = nick_name;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public void setSub_information(String sub_information) {
        this.sub_information = sub_information;
    }

    public String getNick_and_status() {
        nick_and_status = this.nick_name+"-"+map_settings_auto_statuses.get(this.status);
        return nick_and_status;
    }

}
