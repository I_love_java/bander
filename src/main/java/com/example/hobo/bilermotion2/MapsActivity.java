package com.example.hobo.bilermotion2;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;


import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;
import java.util.TreeMap;
import java.util.logging.Logger;

public class MapsActivity extends FragmentActivity implements View.OnClickListener {

    private Button button_back;
    private Button button_settings;
    private Button button_list_bikers;
    private Button search_biker;
    private TextView text_view;

    SharedPreferences sp;
    private GoogleMap mMap; // Might be null if Google Play services APK is not available.
    private Logger logger = Logger.getLogger("MapsActivity");

    private String nick_marker;

    private Marker user_marker;
    private ArrayList <Bikers> users_nicks_al;


    private TreeMap<Integer,String> map_settings_auto_statuses = new TreeMap<Integer,String>();

    private TreeMap<String, Marker> bikers_map = new TreeMap <String, Marker>();
    private ArrayList<String> users_nicks_status_al = new  ArrayList <String>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        initVariables();
        // get obj SharedPreferences, it work with settings file
        sp = PreferenceManager.getDefaultSharedPreferences(this);
        sp.registerOnSharedPreferenceChangeListener(onSharedPreferenceChangeListener);
        // if need clear settings
        // sp.edit().clear().commit();
        setUpMapIfNeeded();
    }

    void initVariables(){


        //initialized treeMap with auto statuses
        int status_array_length = getResources().getStringArray(R.array.entry_values).length;
        String [] array_values = getResources().getStringArray(R.array.entries);
        String [] array_entry_values = getResources().getStringArray(R.array.entry_values);

        for(int i = 0; i < status_array_length; i++)
        map_settings_auto_statuses.put(Integer.parseInt(array_entry_values[i]), array_values[i]);


        //initialized views
        text_view = (TextView)  findViewById(R.id.textView_for_searched_biker);
        button_settings = (Button) findViewById(R.id.settings_button);
        button_settings.setOnClickListener(this);
        button_back = (Button) findViewById(R.id.button_back);
        button_back.setOnClickListener(this);
        button_list_bikers = (Button) findViewById(R.id.button_list_bikers);
        button_list_bikers.setOnClickListener(this);
        search_biker = (Button) findViewById(R.id.button_search);
        search_biker.setOnClickListener(this);


        logger.info("Variables initialized");
    }

    @Override
    protected void onResume() {
        super.onResume();
        try {
            if (nick_marker!=null) {
                Marker choose_maker = bikers_map.get(nick_marker);
                camMover(choose_maker.getPosition());
                choose_maker.showInfoWindow();
                text_view.setText(getResources().getString(R.string.search));
                nick_marker = null;
            }
            else
            {
                camMover(user_marker.getPosition());
                user_marker.showInfoWindow();
            }

        }
        catch(Exception e){
            logger.info("Exception: biker not choose" + e.toString());
        }
    }

    /**
     * Sets up the map if it is possible to do so (i.e., the Google Play services APK is correctly
     * installed) and the map has not already been instantiated.. This will ensure that we only ever
     * call {@link #setUpMap()} once when {@link #mMap} is not null.
     * <p/>
     * If it isn't installed {@link SupportMapFragment} (and
     * {@link com.google.android.gms.maps.MapView MapView}) will show a prompt for the user to
     * install/update the Google Play services APK on their device.
     * <p/>
     * A user can return to this FragmentActivity after following the prompt and correctly
     * installing/updating/enabling the Google Play services. Since the FragmentActivity may not
     * have been completely destroyed during this process (it is likely that it would only be
     * stopped or paused), {@link #onCreate(Bundle)} may not be called again so we should call this
     * method in {@link #onResume()} to guarantee that it will be called.
     */
    private void setUpMapIfNeeded() {
        // Do a null check to confirm that we have not already instantiated the map.
        if (mMap == null) {
            // Try to obtain the map from the SupportMapFragment.
            mMap = ((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map_viev))
                    .getMap();
            // Check if we were successful in obtaining the map.
            if (mMap != null) {
                setUpMap();
            }
        }

    }


    /**
     * This is where we can add markers or lines, add listeners or move the camera. In this case, we
     * just add a marker near Africa.
     * <p/>
     * This should only be called once and when we are sure that {@link #mMap} is not null.
     */
    private void setUpMap() {
        mMap.clear();

        //create user marker and set cursor on user
        user_marker = createUserMarker(new Bikers());

        //parse and return other users
        users_nicks_al = parseAndWriteUsers();

        createSetMarkers(users_nicks_al);

        camMover(user_marker.getPosition());
    }

    private Marker createUserMarker(Bikers user_info){
       Marker user_g_map_marker;

       user_info.setCoordinate(new LatLng(46.4538195, 30.4358351));
       user_info.setNick_name(sp.getString("nick_name", "Unknown user"));
       user_info.setSub_information(sp.getString("Text_status_of_user", "Unknown status"));
       user_info.setStatus(Integer.parseInt(sp.getString("status_list", "240")));

       user_g_map_marker = mMap.addMarker(new MarkerOptions()
               .position(user_info.getCoordinate())
               .title(user_info.getNick_name())
               .snippet(user_info.getSub_information())
               .icon(BitmapDescriptorFactory.defaultMarker(user_info.getStatus())));

        return user_g_map_marker;
   }

    private  ArrayList <Bikers> parseAndWriteUsers(){
        ArrayList <Bikers> users_al = new ArrayList <Bikers> ();
        users_al.add(new Bikers(new LatLng(46.7608195, 30.8358351), 120, map_settings_auto_statuses, "Настя", "Еду на заднем"));
        users_al.add(new Bikers(new LatLng(46.4308195, 30.5358351), 240, map_settings_auto_statuses, "ВикаВика", "Скутеристам не беспокоить"));
        users_al.add(new Bikers(new LatLng(46.2908195, 30.9358351), 0,   map_settings_auto_statuses, "Ghost Rider", "Потерял глушитель!"));
        users_al.add(new Bikers(new LatLng(46.4118195, 30.3358351), 240, map_settings_auto_statuses, "Yamaha R1", "Гоняюсь на деньги"));
        users_al.add(new Bikers(new LatLng(46.4508195, 30.4358351), 240, map_settings_auto_statuses, "Cheburechek", "Продам шенков Фокстерьера"));
        users_al.add(new Bikers(new LatLng(46.4808195, 30.5358351), 240, map_settings_auto_statuses, "Вано", "Куплю бу хулахуп"));
        users_al.add(new Bikers(new LatLng(46.4808195, 30.4358351), 120, map_settings_auto_statuses, "Кулибин", "Сел аккум((("));

       return users_al;
   }

    private void camMover(LatLng latLng){
        CameraUpdate location = CameraUpdateFactory.newLatLngZoom(latLng, 14);
        mMap.animateCamera(location);
    }

    private  void createSetMarkers(ArrayList<Bikers> users_al){

        users_nicks_status_al.clear();
        bikers_map.clear();
        for (Bikers biker : users_al) {

            //write to string var biker nicks with statuses and put it to ArrayList
            users_nicks_status_al.add(biker.getNick_and_status());

            //Create marker
            Marker marker = mMap.addMarker(new MarkerOptions()
                    .position(biker.getCoordinate())
                    .title(biker.getNick_name())
                    .snippet(biker.getSub_information())
                    .icon(BitmapDescriptorFactory.defaultMarker(biker.getStatus())));

            //Put to map: key ->users nicks , value-> marker
            bikers_map.put(biker.getNick_and_status(), marker);
        }
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.button_back:
                camMover(user_marker.getPosition());
                user_marker.showInfoWindow();
                break;

            case R.id.button_list_bikers:
                Intent intent = new Intent(MapsActivity.this, Activity_with_bikers.class);
                intent.putExtra("USERS_LIST", users_nicks_status_al);
                startActivityForResult(intent,1);
                break;

            case R.id.settings_button:
                Intent intent2 = new Intent(MapsActivity.this, PrefActivity.class);
                startActivity(intent2);
                break;

            case R.id.button_search:

                break;
        }
    }


    SharedPreferences.OnSharedPreferenceChangeListener onSharedPreferenceChangeListener = new SharedPreferences.OnSharedPreferenceChangeListener() {
        @Override
        public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
            sp=sharedPreferences;
            setUpMap();
        }
    };

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            nick_marker = data.getStringExtra("NICK");
            text_view.setText(nick_marker);
        }
    }
}
